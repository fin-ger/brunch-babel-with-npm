module.exports = {
    files: {
        javascripts: {
            joinTo: {
                'vendor.js': /^(?!app)/, // Files that are not in `app` dir.
                'app.js': /^app/,
            }
        },
        stylesheets: {
            joinTo: 'app.css',
        },
    },

    modules: {
        wrapper: false,
        definition: "amd",
    },

    npm: {
        enabled: false,
    },

    plugins: {
        babel: {
            presets: ['latest'],
            plugins: ["transform-es2015-modules-amd"],
        },

        copycat: {
            ace: ["node_modules/ace-builds/src"],
        },
    },

    server: {
        noPushState: true,
    },
};
