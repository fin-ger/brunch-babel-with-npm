import ace from "ace/ace";

ace.config.set("packaged", true);
//ace.config.set("basePath", require.toUrl("ace"));
let editor = ace.edit("editor");
editor.setTheme("ace/theme/monokai");
editor.getSession().setMode("ace/mode/c_cpp");
